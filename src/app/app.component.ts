import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { Platform } from '@ionic/angular'
import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { StatusBar } from '@ionic-native/status-bar/ngx'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [
    {title: 'Home', url: '/home', icon: 'home'},
    {title: 'Infrações',url: '/infracoes',icon: 'list'},
    {title: 'Artigos',url: '/artigos',icon: 'list'},
    {title: 'Materiais de apoio',url: '/materiais-apoio',icon: 'list'},
    {title: 'Resoluções',url: '/resolucoes',icon: 'list'},
    {title: 'Etilômetro',url: '/etilometro',icon: 'list'},
    {title: 'Atualizações',url: '/atualizacoes',icon: 'list'},
    {title: 'Minha conta',url: '/minha-conta',icon: 'list'},
    {title: 'Logoff',url: '/infracoes',icon: 'list'}
  ];

  constructor(
    private router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp(){
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      
      //localStorage.clear();

      let usuario = JSON.parse( localStorage.getItem('user_app') )
      if(usuario){
        if(usuario.id){
          this.router.navigateByUrl('home')
        }else{
          if(usuario.confirmar){
            this.router.navigateByUrl('confirma-senha')
          }else{
            this.router.navigateByUrl('login')
          }
        }
      }else{
        this.router.navigateByUrl('login')
      }

    });
  }

  sair(){
    localStorage.clear()
    this.router.navigateByUrl('login')
  }
}
