import { Injectable } from  '@angular/core'
import { HttpClient, HttpHeaders} from  '@angular/common/http'
import { tap } from  'rxjs/operators'
import { Observable } from  'rxjs'
import { environment as ENV} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

	api:string = ENV.apiURL

	constructor(private httpClient:HttpClient){}

	// Header
	setHeader(){
		const usap = JSON.parse(localStorage.getItem('user_app'))
		return new HttpHeaders({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + usap.token
		});
	}

	// infrações
	getInfracoes():Observable<any>{
		return this.httpClient.get(`${this.api}/infringements?sort=asc:code`,{ headers: this.setHeader() }).pipe(
			tap(async (res) => {
				await res
			})
		)
	}
	getInfracoesById(id:number):Observable<any>{
		return this.httpClient.get(`${this.api}/infringements/${id}`,{ headers: this.setHeader() }).pipe(
			tap(async (res) => {
				await res
			})
		)
	}

	// artigos
	getArticles():Observable<any>{
		return this.httpClient.get(`${this.api}/articles?sort=asc:code`,{ headers: this.setHeader() }).pipe(
			tap(async (res) => {
				await res
			})
		)
	}
	getArticlesById(id:number):Observable<any>{
		return this.httpClient.get(`${this.api}/articles/${id}`,{ headers: this.setHeader() }).pipe(
			tap(async (res) => {
				await res
			})
		)
	}
	getArticlesByCode(code:any):Observable<any>{
		return this.httpClient.get(`${this.api}/articles/articleByCode/${code}`,{ headers: this.setHeader() }).pipe(
			tap(async (res) => {
				await res
			})
		)
	}
}
