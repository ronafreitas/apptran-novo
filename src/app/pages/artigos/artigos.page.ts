import { Component, OnInit,ViewChild } from '@angular/core'
import { ApiService } from  '../../services/api.service'
import { FiltraService } from '../../services/filtra.service'
import { LoadingController,ToastController,IonInfiniteScroll } from '@ionic/angular'
import { ConnectionService } from 'ng-connection-service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-artigos',
  templateUrl: './artigos.page.html',
  styleUrls: ['./artigos.page.scss'],
})
export class ArtigosPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  loadingcnt:any
  artigos = []
  resultado:boolean=true
  isConnected:boolean = navigator.onLine
  offlineDataArt=[]
  searchCriteria:any
  toastBusca:any
  timerBusca:any
  limitload:number=30

  constructor(
    private api:ApiService,
    public loadingCtrl: LoadingController,
    private connectionService: ConnectionService,
    public toastController: ToastController,
    public filtra:FiltraService,
    public router:Router
    ){}

  ngOnInit(){
    this.preload()
    this.connectionService.monitor().subscribe(isConn => {
      this.isConnected = isConn
      if(isConn){
        let self = this
        setTimeout(function(){
          self.artigos=[]
          self.getArticles()
        }, 2000)
      }
    })
    this.getArticles()
  }

  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.limitload = this.limitload+30
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  getArticles(){
    let self = this
		this.api.getArticles().subscribe(res => {
      this.loadingcnt.dismiss()
			if(res){
        self.resultado=false
        res.forEach(art => {
          if(art){
            let dadosArt = {
              id:art.id,
              name:art.name,
              description:art.description,
              descriptionStrip:art.descriptionStrip
            }
            self.artigos.push(dadosArt)  
          }
        })
        localStorage.setItem('data_artigos', JSON.stringify(self.artigos))
			}else{
        self.resultado=true
				console.log('erro ao requisitar artigos',res)
      }
		},
		e => {
      if(e.status == 401){
        if(self.loadingcnt != undefined){
          self.loadingcnt.dismiss()
        }
        localStorage.clear();
        this.router.navigateByUrl('login')
      }else{
        setTimeout(function(){
          if(self.loadingcnt != undefined){
            self.loadingcnt.dismiss()
            self.resultado=true
          }
        }, 500)
  
        setTimeout(function(){
          self.offlineDataArt = JSON.parse(localStorage.getItem('data_artigos'))
          self.artigos = self.offlineDataArt
          self.resultado=false
        }, 1800)
      }
		})
  }

  busca(e){
    var texbusc = e.target.value.toLowerCase()
    if(texbusc.length > 1){
      this.offlineDataArt = JSON.parse(localStorage.getItem('data_artigos'))
      let afterFilter = this.filtra.searchFor(this.offlineDataArt,texbusc)

      if(this.toastBusca != undefined){
        this.toastBusca.dismiss()
      }

      if(afterFilter.length > 0){
        this.artigos = afterFilter
      }else{
        this.toasBuscaNaoEncontrada()
      }
    }else if(texbusc.length == 1){
    
    }else if(texbusc.length == 0){
      this.offlineDataArt = JSON.parse(localStorage.getItem('data_artigos'))
      this.artigos = this.offlineDataArt
    }
  }

  searchBoxInput(e){
    let self = this
    clearTimeout(self.timerBusca);
    this.timerBusca = setTimeout(function(){
      self.busca(e)
    }, 1400)
  }
  
  searchBoxCancel(e){
    this.offlineDataArt = JSON.parse(localStorage.getItem('data_artigos'))
    this.artigos = this.offlineDataArt
    this.limitload = 30
  }

  detalhes(id:number){
    this.router.navigateByUrl('artigos-detalhes/'+id)
  }

	async preload(){
		this.loadingcnt = await this.loadingCtrl.create({
			spinner: "bubbles",
			message: 'Aguarde...',
			translucent: true,
		});
		return this.loadingcnt.present();
	}
  async toasBuscaNaoEncontrada() {
    this.toastBusca = await this.toastController.create({
      message: 'Nada encontrado',
      duration: 1500,
      position: 'top',
      animated:true,
    });
    this.toastBusca.present();
  }
}
