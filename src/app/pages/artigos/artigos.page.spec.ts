import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtigosPage } from './artigos.page';

describe('ArtigosPage', () => {
  let component: ArtigosPage;
  let fixture: ComponentFixture<ArtigosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtigosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtigosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
