import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { ApiService } from  '../../services/api.service'
import { LoadingController,ToastController } from '@ionic/angular'
@Component({
  selector: 'app-artigos-detalhes',
  templateUrl: './artigos-detalhes.page.html',
  styleUrls: ['./artigos-detalhes.page.scss'],
})
export class ArtigosDetalhesPage implements OnInit {

  loadingcnt:any
  data = {}

  saiba_icon:string = 'arrow-dropdown'
  comentarios_icon:string = 'arrow-dropdown'
  artigos_icon:string = 'arrow-dropdown'

  saiba_show:boolean=false
  artigos_show:boolean=false
  comentarios_show:boolean=false

  constructor(
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    public toastController: ToastController,
    private api:ApiService){}

  ngOnInit(){
    if(this.route.snapshot.params){
      let id = this.route.snapshot.params['id']
      this.preload()
      this.getArticlesById(id)
    }
  }

  getArticlesById(id:number){
    if(!id){
      return false
    }
    let self = this
    let artigo_by_id = localStorage.getItem('data_artigo_by_id_'+id)
    if(artigo_by_id){
      this.data = JSON.parse(artigo_by_id)
      setTimeout(function(){
        if(self.loadingcnt != undefined){
          self.loadingcnt.dismiss()
        }
      }, 500)
    }else{
      this.api.getArticlesById(id).subscribe(res => {
        this.loadingcnt.dismiss()
        if(res){
          this.data = res
          localStorage.setItem('data_artigo_by_id_'+id, JSON.stringify(this.data))
        }else{
          console.log('erro ao requisitar artigos',res)
        }
      },
      error => {
        setTimeout(function(){
          if(self.loadingcnt != undefined){
            self.loadingcnt.dismiss()
          }
        }, 500)
      })
    }
  }


  collapse(tp){

    switch(tp){
      //Artigos do CTB
      case 1:
          if(this.artigos_icon == 'arrow-dropdown'){
            this.artigos_icon = 'arrow-dropup'
            this.artigos_show = true
          }else{
            this.artigos_icon ='arrow-dropdown'
            this.artigos_show = false
          }
        break;
      
      //Comentários do Julyver
      case 2:
          if(this.comentarios_icon == 'arrow-dropdown'){
            this.comentarios_icon = 'arrow-dropup'
            this.comentarios_show = true
          }else{
            this.comentarios_icon ='arrow-dropdown'
            this.comentarios_show = false
          }
        break;
      
      //Saiba mais
      case 3:
          if(this.saiba_icon == 'arrow-dropdown'){
            this.saiba_icon = 'arrow-dropup'
            this.saiba_show = true
          }else{
            this.saiba_icon ='arrow-dropdown'
            this.saiba_show = false
          }
        break;
    }
  }

  async preload(){
		this.loadingcnt = await this.loadingCtrl.create({
			spinner: "bubbles",
			message: 'Aguarde...',
			translucent: true,
		});
		return this.loadingcnt.present();
	}
}