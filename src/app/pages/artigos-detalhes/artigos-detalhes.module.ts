import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArtigosDetalhesPage } from './artigos-detalhes.page';

const routes: Routes = [
  {
    path: '',
    component: ArtigosDetalhesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArtigosDetalhesPage]
})
export class ArtigosDetalhesPageModule {}
