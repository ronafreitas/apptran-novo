import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from  '../../services/auth.service'
import { UtilsService } from  '../../services/utils.service'
import { LoadingController,AlertController } from '@ionic/angular'
@Component({
  selector: 'app-esqueci',
  templateUrl: './esqueci.page.html',
  styleUrls: ['./esqueci.page.scss'],
})
export class EsqueciPage{
	formulario:any={email:''}
	loadingcnt:any
	constructor(
		private router: Router, 
		private auth:AuthService,
		public loadingCtrl: LoadingController,
		public alertController: AlertController,
		public utils:UtilsService
		){}

	recuperar(){
		if(this.formulario.email == ''){
			this.presentAlert('Atenção','Obrigatório preencher seu email')
			return false
		}
		if(!this.utils.testaEmail(this.formulario.email)){
			this.presentAlert('Atenção','Verifique seu email e tente novamente')
			return false
		}
		this.preloadProcess()

		this.auth.validEmail(this.formulario.email).subscribe(res => {
			if(res){
				this.auth.resetPassword(this.formulario.email).subscribe(res => {
					this.loadingcnt.dismiss()
					if(res){
						localStorage.setItem("user_app", JSON.stringify({id:false, confirmar:true}))
						this.alertConfirmaEmail()
					}else{
						this.presentAlert('Atenção','Erro ao tentar recuperar sua senha')
					}
				},
				error => {
					this.loadingcnt.dismiss()
					console.log(error)
					if( error.error.code == 404 ){
						this.presentAlert('Atenção','Erro ao tentar recuperar sua senha')
					}
				})
			}else{
				this.loadingcnt.dismiss()
				this.presentAlert('Atenção','Não foi possível recuperar sua senha, tente novamente')
				console.log('erro2',res)
			}
		},
		error => {
			this.loadingcnt.dismiss()
			
			console.log(error)
			if( error.error.code == 400 ){
				this.presentAlert('Atenção','Verifique seu email e tente novamente')
			}
		})
	}


	voltar(){
		this.router.navigate(['/login', {replaceUrl: true}]);
	}

	async preloadProcess(){
		this.loadingcnt = await this.loadingCtrl.create({
			spinner: "bubbles",
			message: 'Aguarde...',
			translucent: true,
		})
		return this.loadingcnt.present()
	}

	async presentAlert(titulo:string,msg:string){
		const alert = await this.alertController.create({
			header: titulo,
			message: msg,
			buttons: ['OK']
		})
		await alert.present()
	}

	async alertConfirmaEmail(){
		let choice
		const alert = await this.alertController.create({
		  header: 'E-mail Enviado',
		  subHeader:'Dentro de instantes você receberá um email com instruções',
		  message: 'Nesse e-mail haverá um código, copie e colé na tela de confirmação a seguir',
		  buttons: [
		    {
		      text: 'Ok',
		      handler: () => {
		      	this.router.navigateByUrl('confirma-senha')
		      }
		    }
		  ]
		});

		await alert.present();

	    await alert.onDidDismiss().then((data) => {
	    	this.router.navigateByUrl('confirma-senha')
	    })
	    return true
	}
}
