import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizacoesPage } from './atualizacoes.page';

describe('AtualizacoesPage', () => {
  let component: AtualizacoesPage;
  let fixture: ComponentFixture<AtualizacoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizacoesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizacoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
