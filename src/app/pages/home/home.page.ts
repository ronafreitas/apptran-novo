import { Component,OnInit } from '@angular/core'
import { MenuController } from '@ionic/angular'
import { Router } from '@angular/router'
import { Platform } from '@ionic/angular'
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private menu: MenuController,public router:Router,private platform: Platform){}
  ngOnInit(){
    /*this.platform.ready().then(() => {
			this.platform.backButton.subscribeWithPriority(0, () => {
				document.addEventListener('backbutton', function (event) {
					navigator['app'].exitApp()
				}, false);
			});
		});*/
  }
  abrirMenu(){
    this.menu.toggle();
  }

  goTo(page:string){
    this.router.navigateByUrl(page)
  }
}
