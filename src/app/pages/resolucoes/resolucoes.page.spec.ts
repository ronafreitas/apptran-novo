import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolucoesPage } from './resolucoes.page';

describe('ResolucoesPage', () => {
  let component: ResolucoesPage;
  let fixture: ComponentFixture<ResolucoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolucoesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolucoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
