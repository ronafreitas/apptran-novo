import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MateriaisApoioPage } from './materiais-apoio.page';

const routes: Routes = [
  {
    path: '',
    component: MateriaisApoioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MateriaisApoioPage]
})
export class MateriaisApoioPageModule {}
