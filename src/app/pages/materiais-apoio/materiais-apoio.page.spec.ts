import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MateriaisApoioPage } from './materiais-apoio.page';

describe('MateriaisApoioPage', () => {
  let component: MateriaisApoioPage;
  let fixture: ComponentFixture<MateriaisApoioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriaisApoioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MateriaisApoioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
