import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from  '../../services/auth.service'
import { UtilsService } from '../../services/utils.service'
import { LoadingController,AlertController } from '@ionic/angular'

@Component({
  selector: 'app-confirma-senha',
  templateUrl: './confirma-senha.page.html',
  styleUrls: ['./confirma-senha.page.scss'],
})
export class ConfirmaSenhaPage{

	formulario:any={codigo:'',senha:'',email:''}
	loadingcnt:any

	constructor(
    private router: Router,
    private auth:AuthService,
    public loadingCtrl: LoadingController,
    public alertController: AlertController,
    public utils:UtilsService
    ){}

	alterarSenha(){
		if(
			this.formulario.codigo == '' ||  
			this.formulario.senha == '' ||  
			this.formulario.email == '' 
			){
			this.presentAlert('Atenção','Todos os campos devem ser preenchidos')
			return false
		}
		if(!this.utils.testaEmail(this.formulario.email)){
			this.presentAlert('Atenção','Verifique seu email e tente novamente')
			return false
		}

		this.preloadProcess()

		this.auth.validEmail(this.formulario.email).subscribe(res => {
			if(res){
				let dataConfirm = {
					"email": this.formulario.email,
					"token": this.formulario.codigo,
					"password": this.formulario.senha
				}
				this.auth.confirmReset(dataConfirm).subscribe(res => {
					this.loadingcnt.dismiss()
					console.log(res)
					if(res){
						localStorage.clear()
						this.alertConfirmAlteraSenha()
					}else{
						this.presentAlert('Atenção','Não foi possível alterar sua senha, tente novamente')
					}
				},
				error => {
					this.loadingcnt.dismiss()
					this.presentAlert('Atenção','Não foi possível alterar sua senha, tente novamente')
				})

			}else{
				this.loadingcnt.dismiss()
				this.presentAlert('Atenção','Não foi possível alterar sua senha, tente novamente')
				console.log('erro2',res)
			}
		},
		error => {
			this.loadingcnt.dismiss()
			
			this.presentAlert('Atenção','Verifique seu email e tente novamente')
			if( error.error.code == 400 ){
				//this.presentAlert('Atenção','Verifique seu email e tente novamente')
			}
		})
	}

	voltar(){
		localStorage.clear()
		this.router.navigate(['/login', {replaceUrl: true}])
	}

	async presentAlert(titulo:string,msg:string){
		const alert = await this.alertController.create({
			header: titulo,
			message: msg,
			buttons: ['OK']
		})
		await alert.present()
	}

	async preloadProcess(){
		this.loadingcnt = await this.loadingCtrl.create({
			spinner: "bubbles",
			message: 'Aguarde...',
			translucent: true,
		})
		return this.loadingcnt.present()
	}

	async alertConfirmAlteraSenha(){
		let choice
		const alert = await this.alertController.create({
		  header: 'Sucesso',
		  message: 'Sua nova senha foi confirmada',
		  buttons: [
		    {
		      text: 'Ok',
		      handler: () => {
		      	this.router.navigateByUrl('login')
		      }
		    }
		  ]
		});

    await alert.present();

    await alert.onDidDismiss().then((data) => {
      this.router.navigateByUrl('login')
    })
    return true
	}

}