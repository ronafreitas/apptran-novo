import { Component, OnInit,ViewChild } from '@angular/core'
import { ApiService } from  '../../services/api.service'
import { FiltraService } from '../../services/filtra.service'
import { LoadingController,ToastController,IonInfiniteScroll } from '@ionic/angular'
import { ConnectionService } from 'ng-connection-service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-infracoes',
  templateUrl: './infracoes.page.html',
  styleUrls: ['./infracoes.page.scss'],
})
export class InfracoesPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  loadingcnt:any
  infracoes = []
  resultado:boolean=true
  isConnected:boolean = navigator.onLine
  offlineDataInf=[]
  searchCriteria:any
  toastBusca:any
  timerBusca:any
  limitload:number=30

  constructor(
    private api:ApiService,
    public loadingCtrl: LoadingController,
    private connectionService: ConnectionService,
    public toastController: ToastController,
    public filtra:FiltraService,
    public router:Router
    ){}

  ngOnInit(){
    this.preload()
    this.connectionService.monitor().subscribe(isConn => {
      this.isConnected = isConn
      if(isConn){
        let self = this
        setTimeout(function(){
          self.infracoes=[]
          self.getInfracoes()
        }, 2000)
      }
    })
    this.getInfracoes()
  }

  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.limitload = this.limitload+30
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  getInfracoes(){
    let self = this
		this.api.getInfracoes().subscribe(res => {
      this.loadingcnt.dismiss()
			if(res){
        self.resultado=false
        res.forEach(inf => {
          if(inf){
            let dadosInf = {
              id:inf.id,
              code:inf.code,
              severity:inf.severity,
              description:inf.description,
              points:inf.points,
              value:inf.value,
              descriptionStrip:inf.descriptionStrip
            }
            self.infracoes.push(dadosInf)  
          }
        })
        localStorage.setItem('data_infracoes', JSON.stringify(self.infracoes))
			}else{
        self.resultado=true
				console.log('erro ao requisitar infracoes',res)
      }
		},
		e => {
      if(e.status == 401){
        if(self.loadingcnt != undefined){
          self.loadingcnt.dismiss()
        }
        localStorage.clear();
        this.router.navigateByUrl('login')
      }else{
        setTimeout(function(){
          if(self.loadingcnt != undefined){
            self.loadingcnt.dismiss()
            self.resultado=true
          }
        }, 500)
  
        setTimeout(function(){
          self.offlineDataInf = JSON.parse(localStorage.getItem('data_infracoes'))
          self.infracoes = self.offlineDataInf
          self.resultado=false
        }, 1800)
      }
		})
  }

  busca(e){
    var texbusc = e.target.value.toLowerCase()
    if(texbusc.length > 1){
      this.offlineDataInf = JSON.parse(localStorage.getItem('data_infracoes'))
      let afterFilter = this.filtra.searchFor(this.offlineDataInf,texbusc)

      if(this.toastBusca != undefined){
        this.toastBusca.dismiss()
      }

      if(afterFilter.length > 0){
        this.infracoes = afterFilter
      }else{
        this.toasBuscaNaoEncontrada()
      }
    }else if(texbusc.length == 1){
      // 
    }else if(texbusc.length == 0){
      this.offlineDataInf = JSON.parse(localStorage.getItem('data_infracoes'))
      this.infracoes = this.offlineDataInf
    }
  }

  searchBoxInput(e){
    let self = this
    clearTimeout(self.timerBusca);
    this.timerBusca = setTimeout(function(){
      self.busca(e)
    }, 1400)
  }
  
  detalhes(id:number){
    this.router.navigateByUrl('infracoes-detalhes/'+id)
  }

  searchBoxCancel(e){
    this.offlineDataInf = JSON.parse(localStorage.getItem('data_infracoes'))
    this.infracoes = this.offlineDataInf
    this.limitload = 30
  }
  
	async preload(){
		this.loadingcnt = await this.loadingCtrl.create({
			spinner: "bubbles",
			message: 'Aguarde...',
			translucent: true,
		});
		return this.loadingcnt.present();
	}
  async toasBuscaNaoEncontrada() {
    this.toastBusca = await this.toastController.create({
      message: 'Nada encontrado',
      duration: 1500,
      position: 'top',
      animated:true,
    });
    this.toastBusca.present();
  }
}
