import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfracoesPage } from './infracoes.page';

describe('InfracoesPage', () => {
  let component: InfracoesPage;
  let fixture: ComponentFixture<InfracoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfracoesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfracoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
