import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtilometroPage } from './etilometro.page';

describe('EtilometroPage', () => {
  let component: EtilometroPage;
  let fixture: ComponentFixture<EtilometroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtilometroPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtilometroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
