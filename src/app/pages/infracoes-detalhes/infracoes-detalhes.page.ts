import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from  '../../services/api.service'
import { LoadingController,ToastController } from '@ionic/angular'
@Component({
  selector: 'app-infracoes-detalhes',
  templateUrl: './infracoes-detalhes.page.html',
  styleUrls: ['./infracoes-detalhes.page.scss'],
})
export class InfracoesDetalhesPage implements OnInit {

  loadingcnt:any
  data={}
  data_artigo={}

  infocomple_icon:string = 'arrow-dropdown'
  saiba_icon:string = 'arrow-dropdown'
  comentarios_icon:string = 'arrow-dropdown'
  artigos_icon:string = 'arrow-dropdown'
  providencias_icon:string = 'arrow-dropdown'

  infocomple_show:boolean=false
  providencias_show:boolean=false
  artigos_show:boolean=false
  comentarios_show:boolean=false
  saiba_show:boolean=false

  constructor(
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    public toastController: ToastController,
    private api:ApiService){}

  ngOnInit(){
    if(this.route.snapshot.params){
      let id = this.route.snapshot.params['id']
      this.preload()
      this.getInfracoesById(id)
    }
  }

  getInfracoesById(id:number){
    if(!id){
      return false
    }
    let self = this
    let infracao = localStorage.getItem('data_infracoes_id_'+id)
    if(infracao){
      this.data = JSON.parse(infracao)
      this.getArticlesByCode(parseInt(this.data['article']))
      setTimeout(function(){
        if(self.loadingcnt != undefined){
          self.loadingcnt.dismiss()
        }
      }, 500)
    }else{
      this.api.getInfracoesById(id).subscribe(res => {
        if(this.loadingcnt != undefined){
          this.loadingcnt.dismiss()
        }
        if(res){
          this.data = res
          this.getArticlesByCode(parseInt(res.article))
          localStorage.setItem('data_infracoes_id_'+id, JSON.stringify(this.data))
        }else{
          console.log('erro ao requisitar',res)
        }
      },
      error => {
        setTimeout(function(){
          if(self.loadingcnt != undefined){
            self.loadingcnt.dismiss()
          }
        }, 500)
      })
    }
  }

  getArticlesByCode(code:any){
    if(!code){
      return false
    }

    let artigo = localStorage.getItem('data_infracoes_artigo_id_'+code)
    if(artigo){
      this.data_artigo = JSON.parse(artigo)
    }else{
      let self = this
      this.api.getArticlesByCode(code).subscribe(res => {
        if(this.loadingcnt != undefined){
          this.loadingcnt.dismiss()
        }
        if(res){
          this.data_artigo = res
          localStorage.setItem('data_infracoes_artigo_id_'+code, JSON.stringify(this.data_artigo))
        }else{
          console.log('erro ao requisitar',res)
        }
      },
      error => {
        setTimeout(function(){
          if(self.loadingcnt != undefined){
            self.loadingcnt.dismiss()
          }
        }, 500)
      })
    }
  }

  collapse(tp){

    switch(tp){
      //Informações complementares
      case 1:
          if(this.infocomple_icon == 'arrow-dropdown'){
            this.infocomple_icon = 'arrow-dropup'
            this.infocomple_show = true
          }else{
            this.infocomple_icon ='arrow-dropdown'
            this.infocomple_show = false
          }
        break;

      //Providências adicionais à autuação
      case 2:
          if(this.providencias_icon == 'arrow-dropdown'){
            this.providencias_icon = 'arrow-dropup'
            this.providencias_show = true
          }else{
            this.providencias_icon ='arrow-dropdown'
            this.providencias_show = false
          }
        break;

      //Artigos do CTB
      case 3:
          if(this.artigos_icon == 'arrow-dropdown'){
            this.artigos_icon = 'arrow-dropup'
            this.artigos_show = true
          }else{
            this.artigos_icon ='arrow-dropdown'
            this.artigos_show = false
          }
        break;
      
      //Comentários do Julyver
      case 4:
          if(this.comentarios_icon == 'arrow-dropdown'){
            this.comentarios_icon = 'arrow-dropup'
            this.comentarios_show = true
          }else{
            this.comentarios_icon ='arrow-dropdown'
            this.comentarios_show = false
          }
        break;
      
      //Saiba mais
      case 5:
          if(this.saiba_icon == 'arrow-dropdown'){
            this.saiba_icon = 'arrow-dropup'
            this.saiba_show = true
          }else{
            this.saiba_icon ='arrow-dropdown'
            this.saiba_show = false
          }
        break;
    }
  }

  async preload(){
		this.loadingcnt = await this.loadingCtrl.create({
			spinner: "bubbles",
			message: 'Aguarde...',
			translucent: true,
		});
		return this.loadingcnt.present();
	}
}