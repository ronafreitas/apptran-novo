import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InfracoesDetalhesPage } from './infracoes-detalhes.page';

const routes: Routes = [
  {
    path: '',
    component: InfracoesDetalhesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InfracoesDetalhesPage]
})
export class InfracoesDetalhesPageModule {}
