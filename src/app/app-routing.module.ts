import { NgModule } from '@angular/core'
import { PreloadAllModules, RouterModule, Routes } from '@angular/router'
import { GuardService } from './services/guard.service'

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [GuardService]},
  { path: 'infracoes', loadChildren: './pages/infracoes/infracoes.module#InfracoesPageModule', canActivate: [GuardService]},
  { path: 'artigos', loadChildren: './pages/artigos/artigos.module#ArtigosPageModule', canActivate: [GuardService]},
  { path: 'materiais-apoio', loadChildren: './pages/materiais-apoio/materiais-apoio.module#MateriaisApoioPageModule', canActivate: [GuardService]},
  { path: 'resolucoes', loadChildren: './pages/resolucoes/resolucoes.module#ResolucoesPageModule', canActivate: [GuardService]},
  { path: 'etilometro', loadChildren: './pages/etilometro/etilometro.module#EtilometroPageModule', canActivate: [GuardService]},
  { path: 'atualizacoes', loadChildren: './pages/atualizacoes/atualizacoes.module#AtualizacoesPageModule', canActivate: [GuardService]},
  { path: 'minha-conta', loadChildren: './pages/minha-conta/minha-conta.module#MinhaContaPageModule', canActivate: [GuardService]},
  { path: 'esqueci', loadChildren: './pages/esqueci/esqueci.module#EsqueciPageModule' },
  { path: 'confirma-senha', loadChildren: './pages/confirma-senha/confirma-senha.module#ConfirmaSenhaPageModule' },
  { path: 'infracoes-detalhes/:id', loadChildren: './pages/infracoes-detalhes/infracoes-detalhes.module#InfracoesDetalhesPageModule' },
  { path: 'artigos-detalhes/:id', loadChildren: './pages/artigos-detalhes/artigos-detalhes.module#ArtigosDetalhesPageModule' }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
